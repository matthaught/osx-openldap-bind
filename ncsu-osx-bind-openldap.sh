#!/bin/sh

# Determine OS version
osvers=$(sw_vers -productVersion | awk -F. '{print $2}')

# Environment settings
ldapserver="ldap.ncsu.edu" 	# Fully qualified DNS of your LDAP server
ldapsearch="ou=accounts,dc=ncsu,dc=edu"  # Base ldap search path

# Fix TLS_REQCERT
sed -i '' -e's/TLS_REQCERT[	| ]*demand/TLS_REQCERT	allow/g' /etc/openldap/ldap.conf

# These variables probably don't need to be changed
# Determing if any directory binding exists

if dscl localhost -list /LDAPv3 | grep . > /dev/null; then
    check4OD=$(dscl localhost -list /LDAPv3 | awk 'NR<2{print $NF}')
    echo "Found LDAP: "$check4OD
else
    echo "No LDAP binding found"
fi
check4AD=$(dscl localhost -list "/Active Directory")
if [[ -n ${check4AD} ]]; then
	echo "Found AD: "$check4AD
else
	echo "No AD binding found"
fi

echo "Binding to LDAP Domain "$ldapserver

if [[ ${osvers} -lt 7 ]]; then
   if [ ! -d '/Library/Preferences/DirectoryService' ]; then
    	echo "mkdir /Library/Preferences/DirectoryService"
   fi

   if [ -f /Library/Preferences/DirectoryService/DSLDAPv3PlugInConfig.plist ]; then
     echo "rm /Library/Preferences/DirectoryService/DSLDAPv3PlugInConfig.plist"
   fi
fi

if [[ ${osvers} -lt 7 ]]; then
/bin/cat > /tmp/$ldapserver.plist << 'NEW_LDAP_BIND'
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
	<key>LDAP PlugIn Version</key>
	<string>DSLDAPv3PlugIn Version 1.5</string>
	<key>LDAP Server Configs</key>
	<array>
		<dict>
			<key>Attribute Type Map</key>
			<array>
				<dict>
					<key>Native Map</key>
					<array>
						<string>cn</string>
					</array>
					<key>Standard Name</key>
					<string>dsAttrTypeStandard:RecordName</string>
				</dict>
				<dict>
					<key>Native Map</key>
					<array>
						<string>createTimestamp</string>
					</array>
					<key>Standard Name</key>
					<string>dsAttrTypeStandard:CreationTimestamp</string>
				</dict>
				<dict>
					<key>Native Map</key>
					<array>
						<string>modifyTimestamp</string>
					</array>
					<key>Standard Name</key>
					<string>dsAttrTypeStandard:ModificationTimestamp</string>
				</dict>
			</array>
			<key>Delay Rebind Try in seconds</key>
			<integer>120</integer>
			<key>Denied SASL Methods</key>
			<array>
				<string>DIGEST-MD5</string>
			</array>
			<key>Enable Use</key>
			<true/>
			<key>Idle Timeout in minutes</key>
			<integer>2</integer>
			<key>LDAP Referrals</key>
			<true/>
			<key>Local Security Level</key>
			<dict>
				<key>No ClearText Authentications</key>
				<false/>
			</dict>
			<key>OpenClose Timeout in seconds</key>
			<integer>15</integer>
			<key>Port Number</key>
			<integer>636</integer>
			<key>Record Type Map</key>
			<array>
				<dict>
					<key>Attribute Type Map</key>
					<array>
						<dict>
							<key>Native Map</key>
							<array>
								<string>uid</string>
								<string>cn</string>
							</array>
							<key>Standard Name</key>
							<string>dsAttrTypeStandard:RecordName</string>
						</dict>
						<dict>
							<key>Native Map</key>
							<array>
								<string>cn</string>
							</array>
							<key>Standard Name</key>
							<string>dsAttrTypeStandard:RealName</string>
						</dict>
						<dict>
							<key>Native Map</key>
							<array>
								<string>uidNumber</string>
							</array>
							<key>Standard Name</key>
							<string>dsAttrTypeStandard:UniqueID</string>
						</dict>
						<dict>
							<key>Native Map</key>
							<array>
								<string>gidNumber</string>
							</array>
							<key>Standard Name</key>
							<string>dsAttrTypeStandard:PrimaryGroupID</string>
						</dict>
						<dict>
							<key>Native Map</key>
							<array>
								<string>homeDirectory</string>
							</array>
							<key>Standard Name</key>
							<string>dsAttrTypeStandard:NFSHomeDirectory</string>
						</dict>
						<dict>
							<key>Native Map</key>
							<array>
								<string>userPassword</string>
							</array>
							<key>Standard Name</key>
							<string>dsAttrTypeStandard:Password</string>
						</dict>
						<dict>
							<key>Native Map</key>
							<array>
								<string>#/bin/bash</string>
							</array>
							<key>Standard Name</key>
							<string>dsAttrTypeStandard:UserShell</string>
						</dict>
						<dict>
							<key>Native Map</key>
							<array>
								<string>description</string>
							</array>
							<key>Standard Name</key>
							<string>dsAttrTypeStandard:Comment</string>
						</dict>
						<dict>
							<key>Native Map</key>
							<array>
								<string>shadowLastChange</string>
							</array>
							<key>Standard Name</key>
							<string>dsAttrTypeStandard:Change</string>
						</dict>
						<dict>
							<key>Native Map</key>
							<array>
								<string>shadowExpire</string>
							</array>
							<key>Standard Name</key>
							<string>dsAttrTypeStandard:Expire</string>
						</dict>
					</array>
					<key>Native Map</key>
					<array>
						<dict>
							<key>Group Object Classes</key>
							<string>OR</string>
							<key>Object Classes</key>
							<array>
								<string>posixAccount</string>
								<string>inetOrgPerson</string>
								<string>shadowAccount</string>
							</array>
							<key>Search Base</key>
							<string>LDAPSEARCH</string>
						</dict>
					</array>
					<key>Standard Name</key>
					<string>dsRecTypeStandard:Users</string>
				</dict>
				<dict>
					<key>Attribute Type Map</key>
					<array>
						<dict>
							<key>Native Map</key>
							<array>
								<string>memberUid</string>
							</array>
							<key>Standard Name</key>
							<string>dsAttrTypeStandard:GroupMembership</string>
						</dict>
						<dict>
							<key>Native Map</key>
							<array>
								<string>memberUid</string>
							</array>
							<key>Standard Name</key>
							<string>dsAttrTypeStandard:Member</string>
						</dict>
						<dict>
							<key>Native Map</key>
							<array>
								<string>gidNumber</string>
							</array>
							<key>Standard Name</key>
							<string>dsAttrTypeStandard:PrimaryGroupID</string>
						</dict>
					</array>
					<key>Native Map</key>
					<array>
						<dict>
							<key>Group Object Classes</key>
							<string>OR</string>
							<key>Object Classes</key>
							<array>
								<string>posixGroup</string>
							</array>
							<key>Search Base</key>
							<string>LDAPSEARCH</string>
						</dict>
					</array>
					<key>Standard Name</key>
					<string>dsRecTypeStandard:Groups</string>
				</dict>
				<dict>
					<key>Attribute Type Map</key>
					<array>
						<dict>
							<key>Native Map</key>
							<array>
								<string>mountDirectory</string>
							</array>
							<key>Standard Name</key>
							<string>dsAttrTypeStandard:VFSLinkDir</string>
						</dict>
						<dict>
							<key>Native Map</key>
							<array>
								<string>mountOption</string>
							</array>
							<key>Standard Name</key>
							<string>dsAttrTypeStandard:VFSOpts</string>
						</dict>
						<dict>
							<key>Native Map</key>
							<array>
								<string>mountType</string>
							</array>
							<key>Standard Name</key>
							<string>dsAttrTypeStandard:VFSType</string>
						</dict>
						<dict>
							<key>Native Map</key>
							<array>
								<string>mountDumpFrequency</string>
							</array>
							<key>Standard Name</key>
							<string>dsAttrTypeStandard:VFSDumpFreq</string>
						</dict>
						<dict>
							<key>Native Map</key>
							<array>
								<string>mountPassNo</string>
							</array>
							<key>Standard Name</key>
							<string>dsAttrTypeStandard:VFSPassNo</string>
						</dict>
					</array>
					<key>Native Map</key>
					<array>
						<dict>
							<key>Group Object Classes</key>
							<string>OR</string>
							<key>Object Classes</key>
							<array>
								<string>mount</string>
							</array>
							<key>Search Base</key>
							<string>LDAPSEARCH</string>
						</dict>
					</array>
					<key>Standard Name</key>
					<string>dsRecTypeStandard:Mounts</string>
				</dict>
				<dict>
					<key>Attribute Type Map</key>
					<array>
						<dict>
							<key>Native Map</key>
							<array>
								<string>cn</string>
							</array>
							<key>Standard Name</key>
							<string>dsAttrTypeStandard:RecordName</string>
						</dict>
						<dict>
							<key>Native Map</key>
							<array>
								<string>cn</string>
							</array>
							<key>Standard Name</key>
							<string>dsAttrTypeStandard:RealName</string>
						</dict>
						<dict>
							<key>Native Map</key>
							<array>
								<string>sn</string>
							</array>
							<key>Standard Name</key>
							<string>dsAttrTypeStandard:LastName</string>
						</dict>
						<dict>
							<key>Native Map</key>
							<array>
								<string>givenName</string>
							</array>
							<key>Standard Name</key>
							<string>dsAttrTypeStandard:FirstName</string>
						</dict>
						<dict>
							<key>Native Map</key>
							<array>
								<string>mail</string>
							</array>
							<key>Standard Name</key>
							<string>dsAttrTypeStandard:EMailAddress</string>
						</dict>
						<dict>
							<key>Native Map</key>
							<array>
								<string>telephoneNumber</string>
							</array>
							<key>Standard Name</key>
							<string>dsAttrTypeStandard:PhoneNumber</string>
						</dict>
						<dict>
							<key>Native Map</key>
							<array>
								<string>homePhone</string>
							</array>
							<key>Standard Name</key>
							<string>dsAttrTypeStandard:HomePhoneNumber</string>
						</dict>
						<dict>
							<key>Native Map</key>
							<array>
								<string>facsimileTelephoneNumber</string>
							</array>
							<key>Standard Name</key>
							<string>dsAttrTypeStandard:FAXNumber</string>
						</dict>
						<dict>
							<key>Native Map</key>
							<array>
								<string>mobile</string>
							</array>
							<key>Standard Name</key>
							<string>dsAttrTypeStandard:MobileNumber</string>
						</dict>
						<dict>
							<key>Native Map</key>
							<array>
								<string>pager</string>
							</array>
							<key>Standard Name</key>
							<string>dsAttrTypeStandard:PagerNumber</string>
						</dict>
						<dict>
							<key>Native Map</key>
							<array>
								<string>street</string>
							</array>
							<key>Standard Name</key>
							<string>dsAttrTypeStandard:AddressLine1</string>
						</dict>
						<dict>
							<key>Native Map</key>
							<array>
								<string>postalAddress</string>
							</array>
							<key>Standard Name</key>
							<string>dsAttrTypeStandard:PostalAddress</string>
						</dict>
						<dict>
							<key>Native Map</key>
							<array>
								<string>street</string>
							</array>
							<key>Standard Name</key>
							<string>dsAttrTypeStandard:Street</string>
						</dict>
						<dict>
							<key>Native Map</key>
							<array>
								<string>l</string>
							</array>
							<key>Standard Name</key>
							<string>dsAttrTypeStandard:City</string>
						</dict>
						<dict>
							<key>Native Map</key>
							<array>
								<string>st</string>
							</array>
							<key>Standard Name</key>
							<string>dsAttrTypeStandard:State</string>
						</dict>
						<dict>
							<key>Native Map</key>
							<array>
								<string>postalCode</string>
							</array>
							<key>Standard Name</key>
							<string>dsAttrTypeStandard:PostalCode</string>
						</dict>
						<dict>
							<key>Native Map</key>
							<array>
								<string>c</string>
							</array>
							<key>Standard Name</key>
							<string>dsAttrTypeStandard:Country</string>
						</dict>
						<dict>
							<key>Native Map</key>
							<array>
								<string>o</string>
							</array>
							<key>Standard Name</key>
							<string>dsAttrTypeStandard:OrganizationName</string>
						</dict>
						<dict>
							<key>Native Map</key>
							<array>
								<string>departmentNumber</string>
							</array>
							<key>Standard Name</key>
							<string>dsAttrTypeStandard:Department</string>
						</dict>
						<dict>
							<key>Native Map</key>
							<array>
								<string>title</string>
							</array>
							<key>Standard Name</key>
							<string>dsAttrTypeStandard:JobTitle</string>
						</dict>
						<dict>
							<key>Native Map</key>
							<array>
								<string>buildingName</string>
							</array>
							<key>Standard Name</key>
							<string>dsAttrTypeStandard:Building</string>
						</dict>
						<dict>
							<key>Native Map</key>
							<array>
								<string>userCertificate;binary</string>
							</array>
							<key>Standard Name</key>
							<string>dsAttrTypeStandard:UserCertificate</string>
						</dict>
						<dict>
							<key>Native Map</key>
							<array>
								<string>userSMIMECertificate</string>
							</array>
							<key>Standard Name</key>
							<string>dsAttrTypeStandard:UserSMIMECertificate</string>
						</dict>
						<dict>
							<key>Native Map</key>
							<array>
								<string>userPKCS12</string>
							</array>
							<key>Standard Name</key>
							<string>dsAttrTypeStandard:UserPKCS12Data</string>
						</dict>
					</array>
					<key>Native Map</key>
					<array>
						<dict>
							<key>Group Object Classes</key>
							<string>OR</string>
							<key>Object Classes</key>
							<array>
								<string>inetOrgPerson</string>
							</array>
							<key>Search Base</key>
							<string>LDAPSEARCH</string>
						</dict>
					</array>
					<key>Standard Name</key>
					<string>dsRecTypeStandard:People</string>
				</dict>
				<dict>
					<key>Attribute Type Map</key>
					<array>
						<dict>
							<key>Native Map</key>
							<array>
								<string>cn</string>
							</array>
							<key>Standard Name</key>
							<string>dsAttrTypeStandard:RecordName</string>
						</dict>
						<dict>
							<key>Native Map</key>
							<array>
								<string>authorityRevocationList;binary</string>
							</array>
							<key>Standard Name</key>
							<string>dsAttrTypeStandard:AuthorityRevocationList</string>
						</dict>
						<dict>
							<key>Native Map</key>
							<array>
								<string>certificateRevocationList;binary</string>
							</array>
							<key>Standard Name</key>
							<string>dsAttrTypeStandard:CertificateRevocationList</string>
						</dict>
						<dict>
							<key>Native Map</key>
							<array>
								<string>crossCertificatePair;binary</string>
							</array>
							<key>Standard Name</key>
							<string>dsAttrTypeStandard:CrossCertificatePair</string>
						</dict>
						<dict>
							<key>Native Map</key>
							<array>
								<string>cACertificate;binary</string>
							</array>
							<key>Standard Name</key>
							<string>dsAttrTypeStandard:CACertificate</string>
						</dict>
					</array>
					<key>Native Map</key>
					<array>
						<dict>
							<key>Group Object Classes</key>
							<string>OR</string>
							<key>Object Classes</key>
							<array>
								<string>certificationAuthority</string>
							</array>
							<key>Search Base</key>
							<string>LDAPSEARCH</string>
						</dict>
					</array>
					<key>Standard Name</key>
					<string>dsRecTypeStandard:CertificateAuthorities</string>
				</dict>
				<dict>
					<key>Attribute Type Map</key>
					<array>
						<dict>
							<key>Native Map</key>
							<array>
								<string>description</string>
							</array>
							<key>Standard Name</key>
							<string>dsAttrTypeStandard:Comment</string>
						</dict>
						<dict>
							<key>Native Map</key>
							<array>
								<string>automountMapName</string>
							</array>
							<key>Standard Name</key>
							<string>dsAttrTypeStandard:RecordName</string>
						</dict>
					</array>
					<key>Native Map</key>
					<array>
						<dict>
							<key>Group Object Classes</key>
							<string>OR</string>
							<key>Object Classes</key>
							<array>
								<string>automountMap</string>
							</array>
							<key>Search Base</key>
							<string>LDAPSEARCH</string>
						</dict>
					</array>
					<key>Standard Name</key>
					<string>dsRecTypeStandard:AutomountMap</string>
				</dict>
				<dict>
					<key>Attribute Type Map</key>
					<array>
						<dict>
							<key>Native Map</key>
							<array>
								<string>description</string>
							</array>
							<key>Standard Name</key>
							<string>dsAttrTypeStandard:Comment</string>
						</dict>
						<dict>
							<key>Native Map</key>
							<array>
								<string>automountInformation</string>
							</array>
							<key>Standard Name</key>
							<string>dsAttrTypeStandard:AutomountInformation</string>
						</dict>
						<dict>
							<key>Native Map</key>
							<array>
								<string>automountKey</string>
							</array>
							<key>Standard Name</key>
							<string>dsAttrTypeStandard:RecordName</string>
						</dict>
					</array>
					<key>Native Map</key>
					<array>
						<dict>
							<key>Group Object Classes</key>
							<string>OR</string>
							<key>Object Classes</key>
							<array>
								<string>automount</string>
							</array>
							<key>Search Base</key>
							<string>LDAPSEARCH</string>
						</dict>
					</array>
					<key>Standard Name</key>
					<string>dsRecTypeStandard:Automount</string>
				</dict>
			</array>
			<key>SSL</key>
			<true/>
			<key>Search Timeout in seconds</key>
			<integer>120</integer>
			<key>Server</key>
			<string>LDAPSERVER</string>
			<key>Server Mappings</key>
			<false/>
			<key>Supported Security Level</key>
			<dict>
				<key>Man In The Middle</key>
				<true/>
				<key>No ClearText Authentications</key>
				<true/>
				<key>Packet Encryption</key>
				<true/>
				<key>Packet Signing</key>
				<true/>
			</dict>
			<key>Template Name</key>
			<string>RFC 2307 (Unix)</string>
			<key>Template Search Base Suffix</key>
			<string>LDAPSEARCH</string>
			<key>Template Version</key>
			<string>10.4</string>
			<key>UI Name</key>
			<string>LDAPSERVER</string>
			<key>Use DNS replicas</key>
			<false/>
		</dict>
	</array>
	<key>Service Principals to Create</key>
	<string>host,afpserver,cifs,vnc</string>
</dict>
</plist>
NEW_LDAP_BIND

sed -i '' -e"s/LDAPSERVER/${ldapserver}/g" /tmp/$ldapserver.plist
sed -i '' -e"s/LDAPSEARCH/${ldapsearch}/g" /tmp/$ldapserver.plist

  if [ -f /Library/Preferences/DirectoryService/DSLDAPv3PlugInConfig.plist ]; then
     rm /Library/Preferences/DirectoryService/DSLDAPv3PlugInConfig.plist
     mv /tmp/$ldapserver.plist /Library/Preferences/DirectoryService/DSLDAPv3PlugInConfig.plist
  fi

  echo "Killing DirectoryService"
  killall DirectoryService
  
  echo "Giving Directory Services some time to reload..."
  sleep 10

  
  echo "Killing DirectoryService"
  killall DirectoryService

fi


if [[ ${osvers} -ge 7 ]]; then
	if [ ! -d /Library/Preferences/OpenDirectory/Configurations/LDAPv3 ]; then
    	mkdir /Library/Preferences/OpenDirectory/Configurations/LDAPv3
	fi

	if [ -f /Library/Preferences/OpenDirectory/Configurations/LDAPv3/$ldapserver.plist ]; then
    	mv /Library/Preferences/OpenDirectory/Configurations/LDAPv3/$ldapserver.plist /tmp/config_$ldapserver.plist
	fi
fi

if [[ ${osvers} -ge 7 ]]; then
/bin/cat > /tmp/$ldapserver.plist << 'NEW_LDAP_BIND'
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
        <key>description</key>
        <string>LDAPSERVER</string>
        <key>mappings</key>
        <dict>
                <key>attributes</key>
                <array>
                        <string>objectClass</string>
                </array>
                <key>function</key>
                <string>ldap:translate_recordtype</string>
                <key>recordtypes</key>
                <dict>
                        <key>dsRecTypeStandard:Automount</key>
                        <dict>
                                <key>attributetypes</key>
                                <dict>
                                        <key>dsAttrTypeStandard:AutomountInformation</key>
                                        <dict>
                                                <key>native</key>
                                                <string>automountInformation</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:Comment</key>
                                        <dict>
                                                <key>native</key>
                                                <string>description</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:CreationTimestamp</key>
                                        <dict>
                                                <key>native</key>
                                                <string>createTimestamp</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:ModificationTimestamp</key>
                                        <dict>
                                                <key>native</key>
                                                <string>modifyTimestamp</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:RecordName</key>
                                        <dict>
                                                <key>native</key>
                                                <string>automountKey</string>
                                        </dict>
                                </dict>
                                <key>info</key>
                                <dict>
                                        <key>Group Object Classes</key>
                                        <string>OR</string>
                                        <key>Object Classes</key>
                                        <array>
                                                <string>automount</string>
                                        </array>
                                        <key>Search Base</key>
                                        <string>LDAPSEARCH</string>
                                </dict>
                        </dict>
                        <key>dsRecTypeStandard:AutomountMap</key>
                        <dict>
                                <key>attributetypes</key>
                                <dict>
                                        <key>dsAttrTypeStandard:Comment</key>
                                        <dict>
                                                <key>native</key>
                                                <string>description</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:CreationTimestamp</key>
                                        <dict>
                                                <key>native</key>
                                                <string>createTimestamp</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:ModificationTimestamp</key>
                                        <dict>
                                                <key>native</key>
                                                <string>modifyTimestamp</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:RecordName</key>
                                        <dict>
                                                <key>native</key>
                                                <string>automountMapName</string>
                                        </dict>
                                </dict>
                                <key>info</key>
                                <dict>
                                        <key>Group Object Classes</key>
                                        <string>OR</string>
                                        <key>Object Classes</key>
                                        <array>
                                                <string>automountMap</string>
                                        </array>
                                        <key>Search Base</key>
                                        <string>LDAPSEARCH</string>
                                </dict>
                        </dict>
                        <key>dsRecTypeStandard:CertificateAuthorities</key>
                        <dict>
                                <key>attributetypes</key>
                                <dict>
                                        <key>dsAttrTypeStandard:AuthorityRevocationList</key>
                                        <dict>
                                                <key>native</key>
                                                <string>authorityRevocationList;binary</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:CACertificate</key>
                                        <dict>
                                                <key>native</key>
                                                <string>cACertificate;binary</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:CertificateRevocationList</key>
                                        <dict>
                                                <key>native</key>
                                                <string>certificateRevocationList;binary</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:CreationTimestamp</key>
                                        <dict>
                                                <key>native</key>
                                                <string>createTimestamp</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:CrossCertificatePair</key>
                                        <dict>
                                                <key>native</key>
                                                <string>crossCertificatePair;binary</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:ModificationTimestamp</key>
                                        <dict>
                                                <key>native</key>
                                                <string>modifyTimestamp</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:RecordName</key>
                                        <dict>
                                                <key>native</key>
                                                <string>cn</string>
                                        </dict>
                                </dict>
                                <key>info</key>
                                <dict>
                                        <key>Group Object Classes</key>
                                        <string>OR</string>
                                        <key>Object Classes</key>
                                        <array>
                                                <string>certificationAuthority</string>
                                        </array>
                                        <key>Search Base</key>
                                        <string>LDAPSEARCH</string>
                                </dict>
                        </dict>
                        <key>dsRecTypeStandard:Groups</key>
                        <dict>
                                <key>attributetypes</key>
                                <dict>
                                        <key>dsAttrTypeStandard:CreationTimestamp</key>
                                        <dict>
                                                <key>native</key>
                                                <string>createTimestamp</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:GroupMembership</key>
                                        <dict>
                                                <key>native</key>
                                                <string>memberUid</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:Member</key>
                                        <dict>
                                                <key>native</key>
                                                <string>memberUid</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:ModificationTimestamp</key>
                                        <dict>
                                                <key>native</key>
                                                <string>modifyTimestamp</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:PrimaryGroupID</key>
                                        <dict>
                                                <key>native</key>
                                                <string>gidNumber</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:RecordName</key>
                                        <dict>
                                                <key>native</key>
                                                <string>cn</string>
                                        </dict>
                                </dict>
                                <key>info</key>
                                <dict>
                                        <key>Group Object Classes</key>
                                        <string>OR</string>
                                        <key>Object Classes</key>
                                        <array>
                                                <string>posixGroup</string>
                                        </array>
                                        <key>Search Base</key>
                                        <string>LDAPSEARCH</string>
                                </dict>
                        </dict>
                        <key>dsRecTypeStandard:Mounts</key>
                        <dict>
                                <key>attributetypes</key>
                                <dict>
                                        <key>dsAttrTypeStandard:CreationTimestamp</key>
                                        <dict>
                                                <key>native</key>
                                                <string>createTimestamp</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:ModificationTimestamp</key>
                                        <dict>
                                                <key>native</key>
                                                <string>modifyTimestamp</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:RecordName</key>
                                        <dict>
                                                <key>native</key>
                                                <string>cn</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:VFSDumpFreq</key>
                                        <dict>
                                                <key>native</key>
                                                <string>mountDumpFrequency</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:VFSLinkDir</key>
                                        <dict>
                                                <key>native</key>
                                                <string>mountDirectory</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:VFSOpts</key>
                                        <dict>
                                                <key>native</key>
                                                <string>mountOption</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:VFSPassNo</key>
                                        <dict>
                                                <key>native</key>
                                                <string>mountPassNo</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:VFSType</key>
                                        <dict>
                                                <key>native</key>
                                                <string>mountType</string>
                                        </dict>
                                </dict>
                                <key>info</key>
                                <dict>
                                        <key>Group Object Classes</key>
                                        <string>OR</string>
                                        <key>Object Classes</key>
                                        <array>
                                                <string>mount</string>
                                        </array>
                                        <key>Search Base</key>
                                        <string>LDAPSEARCH</string>
                                </dict>
                        </dict>
                        <key>dsRecTypeStandard:OrganizationalUnit</key>
                        <dict>
                                <key>attributetypes</key>
                                <dict>
                                        <key>dsAttrTypeStandard:AddressLine1</key>
                                        <dict>
                                                <key>native</key>
                                                <string>street</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:City</key>
                                        <dict>
                                                <key>native</key>
                                                <string>l</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:Comment</key>
                                        <dict>
                                                <key>native</key>
                                                <string>description</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:Country</key>
                                        <dict>
                                                <key>native</key>
                                                <string>c</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:FAXNumber</key>
                                        <dict>
                                                <key>native</key>
                                                <string>facsimileTelephoneNumber</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:Password</key>
                                        <dict>
                                                <key>native</key>
                                                <string>userPassword</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:PhoneNumber</key>
                                        <dict>
                                                <key>native</key>
                                                <string>telephoneNumber</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:PostalAddress</key>
                                        <dict>
                                                <key>native</key>
                                                <string>postalAddress</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:PostalCode</key>
                                        <dict>
                                                <key>native</key>
                                                <string>postalCode</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:RealName</key>
                                        <dict>
                                                <key>native</key>
                                                <string>cn</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:RecordName</key>
                                        <dict>
                                                <key>native</key>
                                                <string>ou</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:State</key>
                                        <dict>
                                                <key>native</key>
                                                <string>st</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:Street</key>
                                        <dict>
                                                <key>native</key>
                                                <string>street</string>
                                        </dict>
                                </dict>
                                <key>info</key>
                                <dict>
                                        <key>Group Object Classes</key>
                                        <string>OR</string>
                                        <key>Object Classes</key>
                                        <array>
                                                <string>organizationalUnit</string>
                                        </array>
                                        <key>Search Base</key>
                                        <string>LDAPSEARCH</string>
                                </dict>
                        </dict>
                        <key>dsRecTypeStandard:People</key>
                        <dict>
                                <key>attributetypes</key>
                                <dict>
                                        <key>dsAttrTypeStandard:AddressLine1</key>
                                        <dict>
                                                <key>native</key>
                                                <string>street</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:Building</key>
                                        <dict>
                                                <key>native</key>
                                                <string>buildingName</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:City</key>
                                        <dict>
                                                <key>native</key>
                                                <string>l</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:Country</key>
                                        <dict>
                                                <key>native</key>
                                                <string>c</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:CreationTimestamp</key>
                                        <dict>
                                                <key>native</key>
                                                <string>createTimestamp</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:Department</key>
                                        <dict>
                                                <key>native</key>
                                                <string>departmentNumber</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:EMailAddress</key>
                                        <dict>
                                                <key>native</key>
                                                <string>mail</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:FAXNumber</key>
                                        <dict>
                                                <key>native</key>
                                                <string>facsimileTelephoneNumber</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:FirstName</key>
                                        <dict>
                                                <key>native</key>
                                                <string>givenName</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:HomePhoneNumber</key>
                                        <dict>
                                                <key>native</key>
                                                <string>homePhone</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:JobTitle</key>
                                        <dict>
                                                <key>native</key>
                                                <string>title</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:LastName</key>
                                        <dict>
                                                <key>native</key>
                                                <string>sn</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:MobileNumber</key>
                                        <dict>
                                                <key>native</key>
                                                <string>mobile</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:ModificationTimestamp</key>
                                        <dict>
                                                <key>native</key>
                                                <string>modifyTimestamp</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:OrganizationName</key>
                                        <dict>
                                                <key>native</key>
                                                <string>o</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:PagerNumber</key>
                                        <dict>
                                                <key>native</key>
                                                <string>pager</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:PhoneNumber</key>
                                        <dict>
                                                <key>native</key>
                                                <string>telephoneNumber</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:PostalAddress</key>
                                        <dict>
                                                <key>native</key>
                                                <string>postalAddress</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:PostalCode</key>
                                        <dict>
                                                <key>native</key>
                                                <string>postalCode</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:RealName</key>
                                        <dict>
                                                <key>native</key>
                                                <string>cn</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:RecordName</key>
                                        <dict>
                                                <key>native</key>
                                                <string>cn</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:State</key>
                                        <dict>
                                                <key>native</key>
                                                <string>st</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:Street</key>
                                        <dict>
                                                <key>native</key>
                                                <string>street</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:UserCertificate</key>
                                        <dict>
                                                <key>native</key>
                                                <string>userCertificate;binary</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:UserPKCS12Data</key>
                                        <dict>
                                                <key>native</key>
                                                <string>userPKCS12</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:UserSMIMECertificate</key>
                                        <dict>
                                                <key>native</key>
                                                <string>userSMIMECertificate</string>
                                        </dict>
                                </dict>
                                <key>info</key>
                                <dict>
                                        <key>Group Object Classes</key>
                                        <string>OR</string>
                                        <key>Object Classes</key>
                                        <array>
                                                <string>inetOrgPerson</string>
                                        </array>
                                        <key>Search Base</key>
                                        <string>LDAPSEARCH</string>
                                </dict>
                        </dict>
                        <key>dsRecTypeStandard:Users</key>
                        <dict>
                                <key>attributetypes</key>
                                <dict>
                                        <key>dsAttrTypeStandard:Change</key>
                                        <dict>
                                                <key>native</key>
                                                <string>shadowLastChange</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:Comment</key>
                                        <dict>
                                                <key>native</key>
                                                <string>description</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:CreationTimestamp</key>
                                        <dict>
                                                <key>native</key>
                                                <string>createTimestamp</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:Expire</key>
                                        <dict>
                                                <key>native</key>
                                                <string>shadowExpire</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:ModificationTimestamp</key>
                                        <dict>
                                                <key>native</key>
                                                <string>modifyTimestamp</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:NFSHomeDirectory</key>
                                        <dict>
                                                <key>native</key>
                                                <string>homeDirectory</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:Password</key>
                                        <dict>
                                                <key>native</key>
                                                <string>userPassword</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:PrimaryGroupID</key>
                                        <dict>
                                                <key>native</key>
                                                <string>gidNumber</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:RealName</key>
                                        <dict>
                                                <key>native</key>
                                                <string>cn</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:RecordName</key>
                                        <dict>
                                                <key>native</key>
                                                <string>uid</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:UniqueID</key>
                                        <dict>
                                                <key>native</key>
                                                <string>uidNumber</string>
                                        </dict>
                                        <key>dsAttrTypeStandard:UserShell</key>
                                        <dict>
                                                <key>native</key>
                                                <string>#/bin/bash</string>
                                        </dict>
                                </dict>
                                <key>info</key>
                                <dict>
                                        <key>Group Object Classes</key>
                                        <string>OR</string>
                                        <key>Object Classes</key>
                                        <array>
                                                <string>posixAccount</string>
                                                <string>inetOrgPerson</string>
                                                <string>shadowAccount</string>
                                        </array>
                                        <key>Search Base</key>
                                        <string>LDAPSEARCH</string>
                                </dict>
                        </dict>
                </dict>
        </dict>
        <key>module options</key>
        <dict>
                <key>AppleODClient</key>
                <dict>
                        <key>Server Mappings</key>
                        <false/>
                </dict>
                <key>ldap</key>
                <dict>
                        <key>Denied SASL Methods</key>
                        <array>
                                <string>DIGEST-MD5</string>
                                <string>NTLM</string>
                                <string>GSSAPI</string>
                                <string>CRAM-MD5</string>
                                <string>DIGEST-MD5</string>
                        </array>
                        <key>LDAP Referrals</key>
                        <true/>
                        <key>Template Search Base Suffix</key>
                        <string>LDAPSEARCH</string>
                        <key>Use DNS replicas</key>
                        <false/>
                </dict>
        </dict>
        <key>node name</key>
        <string>/LDAPv3/LDAPSERVER</string>
        <key>options</key>
        <dict>
                <key>connection idle disconnect</key>
                <integer>60</integer>
                <key>connection setup timeout</key>
                <integer>10</integer>
                <key>destination</key>
                <dict>
                        <key>host</key>
                        <string>LDAPSERVER</string>
                        <key>other</key>
                        <string>ldaps</string>
                        <key>port</key>
                        <integer>636</integer>
                </dict>
                <key>man-in-the-middle</key>
                <false/>
                <key>no cleartext authentication</key>
                <true/>
                <key>packet encryption</key>
                <integer>3</integer>
                <key>packet signing</key>
                <integer>1</integer>
                <key>query timeout</key>
                <integer>30</integer>
        </dict>
        <key>template</key>
        <string>LDAPv3</string>
        <key>trusttype</key>
        <string>anonymous</string>
</dict>
</plist>
NEW_LDAP_BIND

sed -i '' -e"s/LDAPSERVER/${ldapserver}/g" /tmp/$ldapserver.plist
sed -i '' -e"s/LDAPSEARCH/${ldapsearch}/g" /tmp/$ldapserver.plist

    if [ ! -f /Library/Preferences/OpenDirectory/Configurations/LDAPv3/$ldapserver.plist ]; then
    	mv /tmp/$ldapserver.plist /Library/Preferences/OpenDirectory/Configurations/LDAPv3/$ldapserver.plist
    fi
    
    sleep 5

	echo "Killing opendirectoryd"
	killall opendirectoryd

fi

echo "Finished LDAP Binding."
# Give DS a chance to catch up
sleep 5

if [[ ${osvers} -ge 7 ]]; then
	if [[ -n ${check4AD} ]]; then
		echo "Removing AD SearchPath"
		dscl localhost -delete Search CSPSearchPath "/Active Directory/${check4AD}/All Domains"
		dscl localhost -delete Search CSPSearchPath "/Active Directory/${check4AD}"
	fi
    echo "Adding LDAP SearchPath"
    dscl localhost -create / SearchPolicy CSPSearchPath
    dscl localhost -append Search CSPSearchPath '/LDAPv3/'$ldapserver
    if [[ -n ${check4AD} ]]; then
    	echo "Adding AD SearchPath"
	dscl localhost -append Search CSPSearchPath "/Active Directory/${check4AD}"
	dscl localhost -append Search CSPSearchPath "/Active Directory/${check4AD}/All Domains"
    fi
    echo "Killing opendirectoryd"
    killall opendirectoryd
fi

if [[ ${osvers} -lt 7 ]]; then
	if [[ -n ${check4AD} ]]; then
		echo "Removing AD SearchPath"
		dscl localhost -delete Search CSPSearchPath "/Active Directory/${check4AD}"
	fi
	echo "Adding LDAP SearchPath"
	dscl localhost -create / SearchPolicy CSPSearchPath
	dscl localhost -append Search CSPSearchPath '/LDAPv3/'$ldapserver
    if [[ -n ${check4AD} ]]; then
    	echo "Adding AD SearchPath"
    	dscl localhost -append Search CSPSearchPath "/Active Directory/${check4AD}"
    fi
	echo "Killing DirectoryService"
	killall DirectoryService
fi

echo "Now bound to LDAP Domain: "
dscl localhost -list /LDAPv3
echo "With Search Path entries: "
dscl /Search -read / CSPSearchPath | grep /LDAP

exit 0